ARG PYTHON_VERSION="3.6-slim-jessie"
FROM python:$PYTHON_VERSION

COPY requirements.txt requirements.txt

RUN apt-get update -qq && apt-get install -yqq \
    git \
    && rm -rf /var/lib/apt/lists

RUN pip install -r requirements.txt --no-cache-dir

WORKDIR /app
